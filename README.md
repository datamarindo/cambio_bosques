## Cambio de uso de suelo en ciudades en bosques de niebla

### Análisis exploratorio de datos

El propósito de este proyecto es en primer lugar ubicar geoespacialmente y
cuantificar el cambio de cobertura en zonas con vegetación de
bosque mesófilo de montaña -utilizando para esta determinación la capa de vegetación potencial de 
Conabio.

Una vez mapeada y cuantificada, se piensa hacer un análisis de 
políticas públicas de vivienda, reflejadas en la relación habitantes/superficie
construída, así como determinar si la _presencia científica_ es decir, la
cantidad de publicaciones sobre ecología y paisaje de una ciudad se pueden
detectar en alguna medida en las políticas públicas.


## cartografía de la expansión

El primer intento para delimitar las zonas urbanas será el marco geoestadístico 
nacional (MGN), que es un producto aproximadamente anual, disponible desde 2010,
aunque ha cambiado un poco su estructura y las capas que incluye. Este proceso
debe ser sencillo, con las capas de áreas amanzanadas de los MGN.

Imagen comparativa de la cobertura según los MGN de 2010 (capa localidades urbanas)
y 2022 (capa EEl)

<img src="featured.png" width="300" height="170" >

Además se intentará, por lo pronto, con los archivos de openstreetmap, que 
incluyen en su historia las fechas de edición. Este proceso requeriría más labor, pues habría que unir las bases de datos de historiales y de líneas 
(vialidades).

En caso de no dar buenos resultados los métodos anteriores se puede usar
el índice de la superficie construída o algún algoritmo de percepción remota.


